#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

void get_current_time(char* time_str){
    time_t now;
    time(&now);
    struct tm* now_tm = localtime(&now);
    struct timespec now_timespec;
    clock_gettime(CLOCK_MONOTONIC, &now_timespec);
    long milliseconds = now_timespec.tv_nsec / 1000000;

    snprintf(time_str, 200, "%d:%d:%d:%ld", now_tm->tm_hour, now_tm->tm_min, now_tm->tm_sec, milliseconds);
}

void print_pid_and_time(pid_t pid, char* time_string){
    if (pid == 0){
        printf("This is the child process, its pid - %d\n", getpid());
        printf("pid of its parent process - %d\n", getppid());
        printf("time - %s\n", time_string);
    }else if (pid > 0){
        return;
    }else{
        printf("fork() call failed, child not created\n");
    }
}

int main(){
    char time_string[200];
    get_current_time(time_string);

    printf("This is the parent process, its pid - %d\n", getpid());
    printf("time - %s\n", time_string);

    pid_t pid1 = fork();
    print_pid_and_time(pid1, time_string);

    if (pid1 > 0){
        pid_t pid2 = fork();
        print_pid_and_time(pid2, time_string);

        if (pid2 > 0){
            system("ps -x");
        }
    }

    return 0;
}
