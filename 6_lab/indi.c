#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>

#define BUF_SIZE 4096

int message_size = 0;

char* concat(char *s1, char *s2){
    size_t len1 = strlen(s1);
    size_t len2 = strlen(s2);

    char* result = malloc(len1 + len2 + 1);

    memcpy(result, s1, len1);
    memcpy(result + len1, s2, len2 + 1);

    return result;
}

char* appending_slash(char* path){
    if (path[strlen(path) - 1] != '/'){
        path = concat(path, "/");
    }
    return path;
}

char* remove_dots(char* s) {
    char* d = s;
    do {
        while (*d == '.') {
            ++d;
        }
    } while (*s++ = *d++);
    return s;
}

int find(char* src, char* m){
    struct stat;

    int find_count = 0;

    FILE* fd = fopen(src, "r");
    char ch;
    char* current_line = malloc(message_size + 1);
    int calloc_size = 0;
    ch = fgetc(fd);
    while (ch != EOF){
        current_line = concat(current_line, &ch);
        calloc_size++;
        printf("%c", ch);
        if (strcmp(current_line, m) == 0){
            find_count++;
            strcpy(current_line, ".");
            calloc_size = 0;
        }

        if (calloc_size == strlen(m)){
            strcpy(current_line, &ch);
            calloc_size = 0;
        }
        ch = fgetc(fd);
    }

    fclose(fd);
    return find_count;
}


int main(int argc, char *argv[]){
    DIR* dir;
    struct dirent* ent;
    int num_of_proc = 0;

    char* src_directory = argv[1];
    char* m = argv[2];
    int max_proc = atoi(argv[3]);

    char* src_file;
    struct stat stat_src;
    int find_count;

    src_directory = appending_slash(src_directory);

    message_size = strlen(m);

    dir = opendir(src_directory);
    if (dir != NULL){
        ent = readdir(dir);
        while (ent != NULL){
            src_file = concat(src_directory, ent->d_name);

            if (opendir(src_file) == NULL){
                pid_t pid = fork();

                switch (pid){
                    case 0:
                        stat(src_file, &stat_src);
                        find_count = find(src_file, m);
                        printf(" Pid: %i; Path: %s; Viewed: %ld bytes; Find: %d\n", getpid(), src_file, stat_src.st_size, find_count);
                        exit(0);
                    default:
                        num_of_proc++;
                        while (num_of_proc > max_proc){
                            wait(NULL);
                            num_of_proc--;
                        }
                        break;
                }
            }

            ent = readdir(dir);
        }
    }

    closedir(dir);
    while (num_of_proc > 0){
        wait(NULL);
        num_of_proc--;
    }

    return 0;
}
